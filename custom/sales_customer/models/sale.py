from odoo import api, fields, models, SUPERUSER_ID, _

class SaleOrderCustomize(models.Model):
    _name = "sale.order.customize"
    # _inherit = ['portal.mixin', 'mail.thread', 'mail.activity.mixin', 'utm.mixin']
    _description = "Sales Order Customize"
    _order = 'date_order desc, id desc'
    name = fields.Char()
    create_date = fields.Datetime()
    date_order = fields.Datetime()
    state = fields.Boolean()
    note = fields.Text()
    amount_total = fields.Integer()
    invoice_status = fields.Boolean()
    invoice_status = fields.Selection([
        ('upselling', 'Upselling Opportunity'),
        ('invoiced', 'Fully Invoiced'),
        ('to invoice', 'To Invoice'),
        ('no', 'Nothing to Invoice')
    ], string='Invoice Status', store=True, readonly=True)